"use strict";

// Variables

const numberPlaceholder = document.querySelector(".number");
const scorePlaceholder = document.querySelector(".score");
const highScorePlaceholder = document.querySelector(".highscore");
const inputValue = document.querySelector(".guess");
const message = document.querySelector(".message");
const checkBtn = document.querySelector(".check");
const againBtn = document.querySelector(".again");
const body = document.querySelector("body");

let secretNumber = Math.trunc(Math.random() * 20 + 1);
let score = 20;
let highScore = 0;

const displayMessage = text => message.textContent = text;

// Check number functionality

checkBtn.addEventListener("click", function () {
  // Turn string into a number
  const guessNumber = Number(document.querySelector(".guess").value);
  console.log(guessNumber, typeof guessNumber);

  if (!guessNumber) {
    displayMessage("🛑 Not a valid number!");
  } else if (guessNumber === secretNumber) {
    displayMessage("🎉 Correct number!");
    numberPlaceholder.textContent = secretNumber;
    body.style.backgroundColor = "#60b347";
    numberPlaceholder.style.width = "30rem";

    if (score > highScore) {
      highScore = score;
    }

    highScorePlaceholder.textContent = highScore;
  } else if (guessNumber !== secretNumber) {
    if (score > 1) {
      displayMessage(guessNumber > secretNumber ? "📈 Too high!" : "📉 Too low!");
      score--;
      scorePlaceholder.textContent = score;
    } else {
      scorePlaceholder.textContent = "0";
      displayMessage("🙊 Game over!");
    }
  }
});

// Reset game state

againBtn.addEventListener("click", () => {
  score = 20;
  scorePlaceholder.textContent = score;
  secretNumber = Math.trunc(Math.random() * 20 + 1);
  inputValue.value = "";
  displayMessage("Start guessing...");
  body.style.backgroundColor = "#222";
  numberPlaceholder.style.width = "15rem";
});
